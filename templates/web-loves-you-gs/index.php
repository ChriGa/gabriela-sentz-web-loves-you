<?php
/**
 * @author   	chris@web-loves-you.com
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts via prefetch oder preload HIER einfügen ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/web-loves-you-gs/fonts/allura-regular-webfont.ttf">
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/web-loves-you-gs/fonts/playfairdisplay-bold-webfont.ttf">	
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/web-loves-you-gs/fonts/playfairdisplay-regular-webfont.ttf">	
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/web-loves-you-gs/fonts/questrial-regular-webfont.ttf">	
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class . ($detect->isMobile() ? "mobile " . $agent : $agent);
?>">

<?php /*CG FB include vorläufig entfernt wg DSGVO 
	<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.8&appId=459204684217310";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		*/ ?>


	<!--web site spinner-->
		<div id="webSiteLoader"></div>
	<!--button back top-->
		<!--<div id="back-top"></div>-->
	<!--page spinner-->
		<div id="pageLoader"><div></div></div>
	<!--background image-->
	<?php if(!$detect->isMobile()) : ?>
		<img class="image_resize bckgr-image" src="templates/web-loves-you-gs/img/bg_pic.jpg" alt="">
	<?php else: /*?>
		<div class="mobBckgr">
			<img class="imgMobBckgr" src="templates/web-loves-you-gs/img/bg_pic-mobile.jpg" alt="">        	
    	</div>	
	<?php */ endif; ?>

	<!-- Body -->
		<div id="wrapper">

			<?php			
			
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');									
					
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	

			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');

			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');			
			
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>								
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript" src="templates/web-loves-you-gs/js/bootstrap.js"></script>
	<script type="text/javascript" src="templates/web-loves-you-gs/js/include_script.js"></script>
<?php 

if ($detect->isMobile() && !$detect->isTablet()) : ?>
	<script type="text/javascript" src="templates/web-loves-you-gs/js/script-mobile-phone.js"></script>
<?php endif; ?>	
<?php if($detect->isTablet()) : ?>	
	<script type="text/javascript" src="templates/web-loves-you-gs/js/script-mobile.js"></script>
<?php endif; ?>
<?php if(!$detect->isMobile()) : ?>
	<script type="text/javascript" src="templates/web-loves-you-gs/js/script.js"></script>
<?php else : ?>
	<script type="text/javascript" src="templates/web-loves-you-gs/js/jquery.backstretch.min.js"></script>
<?php endif; ?>

	<?php if(!$detect->isMobile()) : //CG SVG animation ?>
		<script type="text/javascript" src="templates/web-loves-you-gs/js/vivus.min.js"></script>
		<script type="text/javascript">
			var hi = new Vivus('hi-there', {type: 'delayed', duration: 200, start: 'autostart', dashGap: 2, forceRender: true, pathTimingFunction: Vivus.EASE,
					animTimingFunction: Vivus.LINEAR });

		<?php //CG backgr Blur ! - removeClass muss im Ajax Object ausgeführt werden! siehe script.js ca Zeile 191 ?>
		jQuery('.sf-menu li').click(function() {
			jQuery('.bckgr-image').addClass('blur');

		});
	
		</script>
	<?php else : ?>
	<script>
		jQuery(document).ready(function(){
			
			jQuery('li.item04>a').click(function(e){
				e.preventDefault();
				return false;
			});
			jQuery('#toggle, .bckBut').click(function() {
				jQuery.noConflict();
				jQuery(this).toggleClass('active');
				jQuery('#overlay, #wrapper, .navbarTitle').toggleClass('open');
			});
			jQuery(window).scroll(function() {
				if(jQuery(window).scrollTop() > 50 ) {
					jQuery('.but_back').addClass('btnSmall');
				} else {
					jQuery('.but_back').removeClass('btnSmall');
				}
			});
		<?php //cg bckgr Image handle weil mit CSS Fehler auftreten siehe hier: https://github.com/srobbin/jquery-backstretch ?>
			jQuery.backstretch([
				"templates/web-loves-you-gs/img/bg_pic-mobile.jpg"],
				{centeredX: false});
	});
	</script>		
	<?php endif;  ?>
	<?php if(!$isMobile) : ?>
		<div id="resizeAlarm">
			<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
		</div>
	<?php endif; ?>		
</body>
</html>
