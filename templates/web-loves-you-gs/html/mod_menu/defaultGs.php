<?php
/**
 * @package     	Joomla.Site
 * @subpackage  	mod_menu override
 * @copyright   	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     	GNU General Public License version 2 or later; see LICENSE.txt
 * Modifications	Joomla CSS
 */

defined('_JEXEC') or die;
// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead. ?>
<ul class="sf-menu <?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id') != null)
	{
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
$count = 1;
$fotoShooting = false;
foreach ($list as $i => &$item) :

	$class = 'item0'.$count; // CG submenue li's dürfen $count nicht hochzählen ^^
		if ($item->level < 2) { $count++; if($count == 5){$fotoShooting = true; $class.= " divider dropdown deeper mootools-noconflict parent"; } } 
	if ($item->id == $active_id)
	{
		$class .= ' current';
	}

	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type == 'alias')
	{
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator')
	{
		$class .= ' divider';
	}

	if ($item->deeper) {
		
		if ($item->level < 2) {
			$class .= ' dropdown deeper  mootools-noconflict';
			$first_start = true;
		}
		else {
			$class .= ' dropdown-submenu deeper';
		}
	}

	if ($item->parent)
	{
		$class .= ' parent';
	}

	if (!empty($class))
	{
		$class = ' class="'.trim($class) .'"';
	}

	if ($item->level < 2) { //CG: Submenues dürfen nicht die <li> Klasse haben! 
		echo '<li'.$class.'>';
	}else {
		print '<li>';

	}

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
		case 'heading':
			require JModuleHelper::getLayoutPath('mod_menu', 'default_gs');
			break;

		default:
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	// The next item is deeper.
	if ($item->deeper){
		echo '<ul class="submenu_1 ">';
	}
	elseif ($item->deeper) {
		echo '<ul class="submenu_2 >';
	}
	// The next item is shallower.
	elseif ($item->shallower) {
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	//CG 
	elseif($fotoShooting) {
			print 		'<ul class="submenu_2"><li><a class="" href="/galerie-fotoshooting-gabriela-sentz.html">fotoshooting</a></li></ul>
						<ul class="submenu_3"><li><a class="" href="/galerie-bilder-brautstyling-muenchen.html">brautstyling</a></li></ul>						
					</li>';
			$fotoShooting = false;
	}
	// The next item is on the same level.
	else {
		echo '</li>';	
	}
endforeach;
?></ul>
