<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

//Cg Module rendern in article
	$module_1 	= JModuleHelper::getModules('top2', 'Aktionen');
	$module_2 	= JModuleHelper::getModules('top1', 'Weitere Angebote');
	$document  	= JFactory::getDocument();
	$renderer  	= $document->loadRenderer('module');
	$attribs['style'] = 'none'; 

// mobile detect
		include_once(JPATH_ROOT . '/templates/web-loves-you-gs/Mobile_Detect.php');
		$detect = new Mobile_Detect;

		if($detect->isMobile()) $mobileView = true;	
?>

<div class="cont_bg">
	<div class="inner">
		<?php if(!$mobileView || $detect->isTablet()) : ?>
			<div class="but_back">
	    		<a href="/index.php" class="b bckBut"><img src="templates/web-loves-you-gs/img/arr_back.png" alt="">menu<span></span></a>
	    	</div>
		<?php endif; ?>	    	
		<div class="container">
			<div class="<?php print ($mobileView) ? "row mobRow" : "row" ?>">
				<div class="<?php print(!$mobileView) ? "span4 offset1" : "" ?> introContent">
					<div class="news_module <?php print(!$mobileView) ? "span3" : "" ?>  marg_3">
					<?php if ($module_1) : ?>
							<?php foreach($module_1 as $mod) {
								//print '<h2 class="corr_1">'.$mod->title . '</h2>';
								//print_r($mod); die();
							  		print JModuleHelper::renderModule($mod, $attribs);
							  } ?>
						<?php endif; ?>	
					</div>									
					<?php if ($module_2) : ?>
						<div class="row">
							<div class="service_module <?php print (!$mobileView) ? "span2 offset2" : "" ?>">
								<?php foreach($module_2 as $mod) {
									print '<h3 class="marg_4">'.$mod->title . '</h3>';
								  		print JModuleHelper::renderModule($mod, $attribs);
								  } ?>
							</div>
						</div>
				<?php endif; ?>
				</div>

		<?php // Kategorieblog Ausgabe: ?>

				<div class="<?php print(!$mobileView) ? "span5 offset1" : "" ?> katItems">
					<h3 class="marg_1">
						<?php print $this->escape($this->params->get('page_heading')); ?>
					</h3>
					<ul class="list_2">
					<?php $leadingcount = 0; ?>
						<?php if (!empty($this->lead_items)) : ?>						
								<?php foreach ($this->lead_items as &$item) : ?>
									<li>
										<div class="thumbnail <?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
											itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting">
											<?php
												$this->item = & $item;
												echo $this->loadTemplate('item');
		//print_r($this->item); die();
											?>
										</div>
									</li>
									<?php $leadingcount++; ?>
								<?php endforeach; ?>
						<?php endif; ?>
					</ul>

				</div>
				<div class="clearfix"></div>
			</div>
				<?php if($mobileView && !$detect->isTablet()) : ?>
					<script type="text/javascript">
								jQuery('body').scrollTop(0);
					</script>
				<?php endif; ?>			
		</div>
	</div>
</div>
	<script>

		//list_1-------------------------------------------------
			jQuery('.list_1 > li > a').hover(function(){
			jQuery(this).stop().animate({color:'#ff554e', marginLeft:5}, 300, "easeOutCubic")
			}, function(){;
			jQuery(this).stop().animate({color:'#fff', marginLeft:0}, 300, "easeOutCubic");
			})   
		//end list_1-------------------------------------------------

		jQuery(function(){
			var kontaktUrl = location.pathname;
			if(kontaktUrl != "/kontakt.html") { if(jQuery('.partRight').hasClass('kontakt')) { jQuery('.partRight').removeClass('kontakt'); } } //default
			if(kontaktUrl == "/kontakt.html")  { jQuery('.partRight').addClass('kontakt'); }
		});
		
	</script>


	<?php /* $leadingcount = 0; ?>
	<?php if (!empty($this->lead_items)) : ?>
		<div class="items-leading clearfix">
			<?php foreach ($this->lead_items as &$item) : ?>
				<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
					itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting">
					<?php
					$this->item = & $item;
					echo $this->loadTemplate('item');
					?>
				</div>
				<?php $leadingcount++; ?>
			<?php endforeach; ?>
		</div><!-- end items-leading -->
	<?php endif; ?>

	<?php
	$introcount = (count($this->intro_items));
	$counter = 0;


	<?php if (!empty($this->intro_items)) : ?>
		<?php foreach ($this->intro_items as $key => &$item) : ?>
			<?php $rowcount = ((int) $key % (int) $this->columns) + 1; ?>
			<?php if ($rowcount == 1) : ?>
				<?php $row = $counter / $this->columns; ?>
				<div class="items-row cols-<?php echo (int) $this->columns; ?> <?php echo 'row-' . $row; ?> row-fluid clearfix">
			<?php endif; ?>
			<div class="span<?php echo round((12 / $this->columns)); ?>">
				<div class="item column-<?php echo $rowcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
					itemprop="blogPost" itemscope itemtype="https://schema.org/BlogPosting">
					<?php
					$this->item = & $item;
					echo $this->loadTemplate('item');
					?>
				</div>
				<!-- end item -->
				<?php $counter++; ?>
			</div><!-- end span -->
			<?php if (($rowcount == $this->columns) or ($counter == $introcount)) : ?>
				</div><!-- end row -->
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if (!empty($this->link_items)) : ?>
		<div class="items-more">
			<?php echo $this->loadTemplate('links'); ?>
		</div>
	<?php endif; ?>

	<?php if (!empty($this->children[$this->category->id]) && $this->maxLevel != 0) : ?>
		<div class="cat-children">
			<?php if ($this->params->get('show_category_heading_title_text', 1) == 1) : ?>
				<h3> <?php echo JText::_('JGLOBAL_SUBCATEGORIES'); ?> </h3>
			<?php endif; ?>
			<?php echo $this->loadTemplate('children'); ?> </div>
	<?php endif; ?>
	<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
		<div class="pagination">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
			<?php endif; ?>
			<?php echo $this->pagination->getPagesLinks(); ?> </div>
	<?php endif; ?>
</div>
	*/ ?>
