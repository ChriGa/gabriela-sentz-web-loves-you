<?php
/**
 * @package     Joomla.Site - web-loves-you Override f. GS Template chris@web-loves-you.com
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
	$intrImg = $images->image_intro;
	$fullImg = $images->image_fulltext;
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');

//CG Module rendern in article
	$module_1 	= JModuleHelper::getModules('top2', 'Aktionen');
	$module_2 	= JModuleHelper::getModules('top1', 'Weitere Angebote');
	$module_3 	= JModuleHelper::getModules('priceTable', 'Preisliste');
		if(!empty($module_3)) {$mod3 = true;}
	$document  	= JFactory::getDocument();
	$renderer  	= $document->loadRenderer('module');
	$attribs['style'] = 'none'; 

// mobile detect
		include_once(JPATH_ROOT . '/templates/web-loves-you-gs/Mobile_Detect.php');
		$detect = new Mobile_Detect;

		if($detect->isMobile()) $mobileView = true;
?>

<div class="cont_bg">
	<div class="inner">
		<?php if(!$mobileView || $detect->isTablet()) : ?>
			<div class="but_back">
	    		<a href="index.php" class="b bckBut"><img src="templates/web-loves-you-gs/img/arr_back.png" alt="">menu<span></span></a>
	    	</div>
    	<?php endif; ?>
		<div class="container">
			<div class="<?php print ($mobileView) ? "row mobRow" : "row" ?>">
				<div class="<?php print(!$mobileView) ? "span4 offset1" : ""; ?> introContent">
					<div class="news_module <?php print (!$mobileView) ? "span3" : ""; ?> marg_3">
						<?php if ($module_1) : ?>
							<?php foreach($module_1 as $mod) {
							  		print JModuleHelper::renderModule($mod, $attribs);
							  } ?>
						<?php endif; ?>						
					</div>
					<?php if ($module_2) : ?>
						<div class="row">
							<div class="service_module <?php print(!$mobileView) ? "span2 offset2" : ""; ?>">
								<?php foreach($module_2 as $mod) {
									print '<h3 class="marg_4">'.$mod->title . '</h3>';
								  		print JModuleHelper::renderModule($mod, $attribs);
								  } ?>
							</div>
						</div>
					<?php elseif ($module_3) : ?>						
						<div class="row">
							<div class="service_module priceTable <?php print(!$mobileView) ? "span2 offset2" : ""; ?>">
								<?php foreach($module_3 as $mod) {
										print '<h3 class="marg_4">'.$mod->title . '</h3>';									
								  		print JModuleHelper::renderModule($mod, $attribs);
								  } ?>
							</div>
						</div>						
				<?php else : ?>
					<div class="row">
						<div class="service_module <?php print(!$mobileView) ? "span2 offset2" : " "; ?> ">
								<a class="btn" href="#" onclick="history.go(-1);">zur&uuml;ck</a>
							</div>
						</div>
				<?php endif; ?>
				</div>
			<?php
				/*
				 * CG : start - article tmpl
				 */
			?>
				<div class="<?php print(!$mobileView) ? "span4 offset1" : "";?> itemId-<?php print $this->item->id; ?> itemContent">
					<?php if($this->params->get('show_title')) : ?> 
						<h2 class="marg_1">
							<?php print $this->item->title; ?>
						</h2>
					<?php endif; ?>
<?php //preprint($this->item); die(); ?>
					<?php if(!empty($intrImg)) : ?>
						<?php if($this->item->id == 6) : ?> <?php //CG: Beitrag id-6 intro Image mit Bildergalerie verlinken ?>
							<a href="/galerie-bilder-brautstyling-muenchen.html" title="Hier gelangen Sie direkt zur Bildergalerie">
								<img src="<?php print $intrImg; ?>" alt="<?php print htmlspecialchars($images->image_intro_alt);?>" class="round marg_2">
							</a>
						<?php else :?>
							<img src="<?php print $intrImg; ?>" alt="<?php print htmlspecialchars($images->image_intro_alt);?>" class="round marg_2">
						<?php endif; ?>
					<?php endif; ?>
					<div class="intrTxt marg_2 scroll_1">
						<div class="cmsContent">
							<?php print $this->item->introtext; ?>
							<?php print $this->item->fulltext; ?>
						</div>
					</div>
					<?php if($this->item->id == 22 ) : ?>
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FWellnessKosmetikGabriela%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=459204684217310" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
						</iframe>
					<?php endif;?>
					<?php if(!$mobileView) : // scroll buttons nur ab Desktop View ?>
						<div class="scroll-btns viz_1">
			                <a href="#" data-type="scrollDown" class="btndown"><span></span></a>
			                <a href="#" data-type="scrollUp" class="btnup"><span></span></a>
						</div>	
					<?php endif; ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if($mobileView && !$detect->isTablet()) : ?>
				<script type="text/javascript">
							jQuery('body').scrollTop(0);
				</script>
			<?php endif; ?>
		</div>
	</div>
</div>
	<script>

		//list_1-------------------------------------------------
			jQuery('.list_1 > li > a').hover(function(){
			jQuery(this).stop().animate({color:'#ff554e', marginLeft:5}, 300, "easeOutCubic")
			}, function(){;
			jQuery(this).stop().animate({color:'#fff', marginLeft:0}, 300, "easeOutCubic");
			})   
		//end list_1-------------------------------------------------

		jQuery(function(){
			var kontaktUrl = location.pathname;
			if(kontaktUrl != "/kontakt.html") { if(jQuery('.partRight').hasClass('kontakt')) { jQuery('.partRight').removeClass('kontakt'); } } //default
			if(kontaktUrl == "/kontakt.html")  { jQuery('.partRight').addClass('kontakt'); }
		});
		
	</script>

<?php /*
<div class="XXXXX item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? JFactory::getConfig()->get('language') : $this->item->language; ?>" />
	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1> <?php echo $this->escape($this->params->get('page_heading')); ?> </h1>
	</div>
	<?php endif;
	if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && $this->item->paginationrelative)
	{
		echo $this->item->pagination;
	}
	?>

	<?php // Todo Not that elegant would be nice to group the params ?>
	<?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
	|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author') ); ?>

	<?php if (!$useDefList && $this->print) : ?>
		<div id="pop-print" class="btn hidden-print">
			<?php echo JHtml::_('icon.print_screen', $this->item, $params); ?>
		</div>
		<div class="clearfix"> </div>
	<?php endif; ?>
	<?php if ($params->get('show_title') || $params->get('show_author')) : ?>
	<div class="page-header">
		<?php if ($params->get('show_title')) : ?>
			<h2 itemprop="name">
				<?php echo $this->escape($this->item->title); ?>
			</h2>
		<?php endif; ?>
		<?php if ($this->item->state == 0) : ?>
			<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
		<?php endif; ?>
		<?php if (strtotime($this->item->publish_up) > strtotime(JFactory::getDate())) : ?>
			<span class="label label-warning"><?php echo JText::_('JNOTPUBLISHEDYET'); ?></span>
		<?php endif; ?>
		<?php if ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != JFactory::getDbo()->getNullDate()) : ?>
			<span class="label label-warning"><?php echo JText::_('JEXPIRED'); ?></span>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	<?php if (!$this->print) : ?>
		<?php if ($canEdit || $params->get('show_print_icon') || $params->get('show_email_icon')) : ?>
			<?php echo JLayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item, 'print' => false)); ?>
		<?php endif; ?>
	<?php else : ?>
		<?php if ($useDefList) : ?>
			<div id="pop-print" class="btn hidden-print">
				<?php echo JHtml::_('icon.print_screen', $this->item, $params); ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
		<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
	<?php endif; ?>

	<?php if ($info == 0 && $params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
		<?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>

		<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
	<?php endif; ?>

	<?php // Content is generated by content plugin event "onContentAfterTitle" ?>
	<?php if (!$params->get('show_intro')) : echo $this->item->event->afterDisplayTitle; endif; ?>
	<?php // Content is generated by content plugin event "onContentBeforeDisplay" ?>
	<?php echo $this->item->event->beforeDisplayContent; ?>

	<?php if (isset($urls) && ((!empty($urls->urls_position) && ($urls->urls_position == '0')) || ($params->get('urls_position') == '0' && empty($urls->urls_position)))
		|| (empty($urls->urls_position) && (!$params->get('urls_position')))) : ?>
	<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
	<?php if ($params->get('access-view')):?>
	<?php if (isset($images->image_fulltext) && !empty($images->image_fulltext)) : ?>
	<?php $imgfloat = (empty($images->float_fulltext)) ? $params->get('float_fulltext') : $images->float_fulltext; ?>
	<div class="pull-<?php echo htmlspecialchars($imgfloat); ?> item-image"> <img
	<?php if ($images->image_fulltext_caption):
		echo 'class="caption"' . ' title="' . htmlspecialchars($images->image_fulltext_caption) . '"';
	endif; ?>
	src="<?php echo htmlspecialchars($images->image_fulltext); ?>" alt="<?php echo htmlspecialchars($images->image_fulltext_alt); ?>" itemprop="image"/> </div>
	<?php endif; ?>
	<?php
	if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && !$this->item->paginationrelative):
		echo $this->item->pagination;
	endif;
	?>
	<?php if (isset ($this->item->toc)) :
		echo $this->item->toc;
	endif; ?>
	<div itemprop="articleBody">
		<?php echo $this->item->text; ?>
	</div>

	<?php if ($info == 1 || $info == 2) : ?>
		<?php if ($useDefList) : ?>
			<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
		<?php endif; ?>
		<?php if ($params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
			<?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
			<?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
		<?php endif; ?>
	<?php endif; ?>

	<?php
	if (!empty($this->item->pagination) && $this->item->pagination && $this->item->paginationposition && !$this->item->paginationrelative):
		echo $this->item->pagination;
	?>
	<?php endif; ?>
	<?php if (isset($urls) && ((!empty($urls->urls_position) && ($urls->urls_position == '1')) || ($params->get('urls_position') == '1'))) : ?>
	<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
	<?php // Optional teaser intro text for guests ?>
	<?php elseif ($params->get('show_noauth') == true && $user->get('guest')) : ?>
	<?php echo JLayoutHelper::render('joomla.content.intro_image', $this->item); ?>
	<?php echo JHtml::_('content.prepare', $this->item->introtext); ?>	
<?php // Optional link to let them register to see the whole article. ?>
	<?php if ($params->get('show_readmore') && $this->item->fulltext != null) : ?>
	<?php $menu = JFactory::getApplication()->getMenu(); ?>
	<?php $active = $menu->getActive(); ?>
	<?php $itemId = $active->id; ?>
	<?php $link = new JUri(JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false)); ?>
	<?php $link->setVar('return', base64_encode(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language))); ?>
	<p class="readmore">
		<a href="<?php echo $link; ?>" class="register">
		<?php $attribs = json_decode($this->item->attribs); ?>
		<?php
		if ($attribs->alternative_readmore == null) :
			echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
		elseif ($readmore = $this->item->alternative_readmore) :
			echo $readmore;
			if ($params->get('show_readmore_title', 0) != 0) :
				echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
			endif;
		elseif ($params->get('show_readmore_title', 0) == 0) :
			echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
		else :
			echo JText::_('COM_CONTENT_READ_MORE');
			echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
		endif; ?>
		</a>
	</p>
	<?php endif; ?>
	<?php endif; ?>
	<?php
	if (!empty($this->item->pagination) && $this->item->pagination && $this->item->paginationposition && $this->item->paginationrelative) :
		echo $this->item->pagination;
	?>
	<?php endif; ?>
	<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
	<?php echo $this->item->event->afterDisplayContent; ?>
</div>
*/ ?>