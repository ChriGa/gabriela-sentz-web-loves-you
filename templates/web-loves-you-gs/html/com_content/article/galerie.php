<?php
/**
 * @package     Joomla.Site - web-loves-you Override f. GS Template chris@web-loves-you.com
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
	$intrImg = $images->image_intro;
	$fullImg = $images->image_fulltext;
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');

//CG Module rendern in article
	$module 	= JModuleHelper::getModules('top2', 'Aktionen');
	$document  	= JFactory::getDocument();
	$renderer  	= $document->loadRenderer('module');
	$attribs['style'] = 'none'; 

// mobile detect
		include_once(JPATH_ROOT . '/templates/web-loves-you-gs/Mobile_Detect.php');
		$detect = new Mobile_Detect;

		if($detect->isMobile()) $mobileView = true;		
?>


<div class="cont_bg ">
	<div class="inner">
		<?php if(!$mobileView || $detect->isTablet()) : ?>
			<div class="but_back">
	    		<a href="index.php" class="b bckBut"><img src="templates/web-loves-you-gs/img/arr_back.png" alt="">menu<span></span></a>
	    	</div>
    	<?php endif; ?>
		<div class="container">
			<div class="<?php print($mobileView) ? "row mobRow gallery" : "row" ?>">
				<div class="<?php print(!$mobileView) ? "span4 offset1" : "" ?> introContent">
					<div class="news_module <?php print(!$mobileView) ? "span3" : ""?> marg_3">
						<?php if ($module) : ?>
							<?php foreach($module as $mod) {
							  		print JModuleHelper::renderModule($mod, $attribs);
							  } ?>
						<?php endif; ?>						
					</div>
					<?php if(!$mobileView) : ?>
						<div class="<?php print(!$mobileView) ? "span3 offset2" : "" ?> latePhoto">
							<h3 class="marg_7">Neuestes Foto</h3>
							<div class="latest">
								<a href="<?php print $fullImg; ?>"><img src="<?php print $intrImg; ?>" <?php //CG "neues Bild" im Back-end als Intro - bzw und Deatilbild hinterlegt ?>
									alt="Aktuellstes Bild der Arbeit von Gabriela Sentz" class="round">
								</a>
							</div>	
						</div>
					<?php endif; ?>
				</div>		
			<?php
				/*
				 * CG : start - Galerie tmpl
				 */
			?>
				<div class="itemId-<?php print $this->item->id; ?> itemContent">
					<div class="<?php print(!$mobileView) ? "span4" : "" ?> galHolder">
					<?php if($this->params->get('show_title')) : ?> 
						<h2 class="marg_7">
							<?php print $this->item->title; ?>
						</h2>
					<?php endif; ?>
						<div class="slider1">
							<?php print $this->item->introtext; ?>
						</div>
						<div id="arrows1">
							<a class="prev1"><span></span></a>
							<a class="next1"><span></span></a>
						</div>
					</div>
					<?php if($mobileView) : ?>
						<div class="<?php print(!$mobileView) ? "span3 offset2" : "" ?> latePhoto">
							<h3 class="marg_7">Neuestes Foto</h3>
							<div class="latest">
								<a href="<?php print $fullImg; ?>"><img src="<?php print $intrImg; ?>" <?php //CG "neues Bild" im Back-end als Intro - bzw und Deatilbild hinterlegt ?>
									alt="Aktuellstes Bild der Arbeit von Gabriela Sentz" class="round">
								</a>
							</div>	
						</div>
					<?php endif; ?>					
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if($mobileView && !$detect->isTablet()) : ?>
				<script type="text/javascript">
							jQuery('body').scrollTop(0);
				</script>
			<?php endif; ?>			
		</div>
	</div>
</div>
<script type="text/javaScript">

		jQuery(function(){
			var kontaktUrl = location.pathname;
			if(kontaktUrl != "/kontakt.html") { if(jQuery('.partRight').hasClass('kontakt')) { jQuery('.partRight').removeClass('kontakt'); } } //default
			if(kontaktUrl == "/kontakt.html")  { jQuery('.partRight').addClass('kontakt'); }
		});

	//slider works----------------------------------------------
	 if (jQuery(".slider1").length) {
        jQuery('.slider1').cycle({
            fx: 'scrollHorz',
            speed: 600,
    		timeout: 0,
            next: '.next1',
    		prev: '.prev1',                
    		easing: 'easeInOutExpo',
    		cleartypeNoBg: true ,
            rev:0,
            startingSlide: 0,
            wrap: true
  		})
    };
    var ind = 0;
    var len = jQuery('.nav_item').length;
    jQuery('.nav_item').bind('click',function(){
        ind = jQuery(this).index()-0;
        jQuery('.nav_item').each(function(index,elem){if (index!=(ind)){$(this).removeClass('active');}});
        jQuery(this).addClass('active');
        jQuery('.slider1').cycle(ind);
    });

	jQuery('#arrows1 span').css({opacity:0});

   	jQuery('.next1').hover(function(){
		jQuery(this).find('span').stop(true).animate({opacity:1}, 350, 'easeOutSine');				 
	}, function(){
		jQuery(this).find('span').stop(true).animate({opacity:0}, 350, 'easeOutSine')					 
	})
	
	jQuery('.prev1').hover(function(){
		jQuery(this).find('span').stop(true).animate({opacity:1}, 350, 'easeOutSine');				 
	}, function(){
		jQuery(this).find('span').stop(true).animate({opacity:0}, 350, 'easeOutSine')					 
	})


	//********** gall ***********//
	jQuery('.latest a').attr('rel','appendix')
    .prepend('<span class="sitem_over"><strong></strong></span>')
    jQuery('.latest a').fancybox({
        'transitionIn': 'elastic',
    	'transitionOut': 'elastic',
    	'speedIn': 500,
    	'speedOut': 300,
        'centerOnScroll': true,
        'overlayColor': '#000'
    });
	
	jQuery('.latest a')
    .find('strong').css('top','200px').end()
    .hover(
	        function(){
	            jQuery(this).children('.sitem_over').css({display:'block',opacity:'0'}).stop().animate({'opacity':1}).end() 
	            .find('strong').css({'opacity':0}).stop().animate({'opacity':1,'top':'0'},350,'easeInOutExpo');
   
        },
        function(){
                jQuery(this).children('.sitem_over').stop().animate({'opacity':0},1000,'easeOutQuad',function(){jQuery(this).children('.sitem_over').css({display:'none'})}).end()  
                .find('strong').stop().animate({'opacity':0,'top':'200px'},1000,'easeOutQuad');             
        }
    );

//********** end gall ***********//

jQuery('.slider1 a.imgLink').attr('rel','appendix')
    .prepend('<span class="sitem_over"><strong></strong></span>')
    jQuery('.slider1 a.imgLink').fancybox({
        'transitionIn': 'elastic',
    	'transitionOut': 'elastic',
    	'speedIn': 500,
    	'speedOut': 300,
        'centerOnScroll': true,
        'overlayColor': '#000'
    });
	
	jQuery('.slider1 a.imgLink')
    .find('strong').css('top','200px').end()
    .hover(
        function(){
                jQuery(this).children('.sitem_over').css({display:'block',opacity:'0'}).stop().animate({'opacity':1}).end() 
                .find('strong').css({'opacity':0}).stop().animate({'opacity':1,'top':'0'},350,'easeInOutExpo');
        },
        function(){
                jQuery(this).children('.sitem_over').stop().animate({'opacity':0},1000,'easeOutQuad',function(){jQuery(this).children('.sitem_over').css({display:'none'})}).end()  
                .find('strong').stop().animate({'opacity':0,'top':'200px'},1000,'easeOutQuad');             
        }
    );
</script>	

