<?php
/**
 * @package     Joomla.Site - web-loves-you Override f. GS Template chris@web-loves-you.com
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

// Create shortcuts to some parameters.
$params  = $this->item->params;
$images  = json_decode($this->item->images);
	$intrImg = $images->image_intro;
	$fullImg = $images->image_fulltext;
$urls    = json_decode($this->item->urls);
$canEdit = $params->get('access-edit');
$user    = JFactory::getUser();
$info    = $params->get('info_block_position', 0);
JHtml::_('behavior.caption');

//CG Module rendern in article
	$module_1 	= JModuleHelper::getModules('top2', 'Aktionen');
	$module_2 	= JModuleHelper::getModules('top1', 'Weitere Angebote');
	$module_3 	= JModuleHelper::getModules('position-2', 'Kontaktformular');
	$document  	= JFactory::getDocument();
	$renderer  	= $document->loadRenderer('module');
	$attribs['style'] = 'none'; 

// mobile detect
		include_once(JPATH_ROOT . '/templates/web-loves-you-gs/Mobile_Detect.php');
		$detect = new Mobile_Detect;

		if($detect->isMobile()) $mobileView = true;	

?>

<div class="cont_bg">
	<div class="inner">
		<?php if(!$mobileView || $detect->isTablet()) : ?>
			<div class="but_back">
	    		<a href="index.php" class="b bckBut"><img src="templates/web-loves-you-gs/img/arr_back.png" alt="">menu<span></span></a>
	    	</div>
	    <?php endif; ?>
		<div class="container">
			<div class="<?php print ($mobileView) ? "row mobRow " : "row" ?>">
				<div class="<?php print (!$mobileView) ? "span4 offset1 " : "" ?> introContent">
					<div class="news_module span3 marg_3">
						<?php if ($module_1) : ?>
							<?php foreach($module_1 as $mod) {
								//print '<h2 class="corr_1">'.$mod->title . '</h2>';
								//print_r($mod); die();
							  		print JModuleHelper::renderModule($mod, $attribs);
							  } ?>
						<?php endif; ?>						
					</div>
					<?php if ($module_2) : ?>
						<div class="row">
							<div class="service_module <?php print (!$mobileView) ? "span2 offset2" : "" ?>">
								<?php foreach($module_2 as $mod) {
									print '<h3 class="marg_4">'.$mod->title . '</h3>';
								  		print JModuleHelper::renderModule($mod, $attribs);
								  } ?>
							</div>
						</div>
				<?php else : ?>
					<div class="row">
						<div class="service_module <?php print (!$mobileView) ? "span2 offset2" : "" ?>">
								<a class="btn" href="#" onclick="history.go(-1);">zur&uuml;ck</a>
							</div>
						</div>
				<?php endif; ?>
				</div>
			<?php
				/*
				 * CG : start - article  - kontakt tmpl
				 */
			?>
				<div class="<?php print (!$mobileView) ? "span4 offset1" : "" ?> itemId-<?php print $this->item->id; ?> itemContent">
					<?php if($this->params->get('show_title')) : ?> 
						<h2 class="marg_1">
							<?php print $this->item->title; ?>
						</h2>
					<?php endif; ?>
<?php //preprint($this->item); die(); ?>
					<?php if(!empty($intrImg)) : ?>
						<img src="<?php print $intrImg; ?>" alt="<?php print htmlspecialchars($images->image_intro_alt);?>" class="round marg_2">
					<?php endif; ?>
					<div class="intrTxt marg_2 scroll_1">
						<div>
							<?php print $this->item->introtext; ?>
						</div>
						<?php if ($mobileView && $module_3) : ?>
							<?php foreach($module_3 as $mod) {
							  		print JModuleHelper::renderModule($mod, $attribs);
							  } ?>
						<?php endif; ?>							
					</div>					
				</div>
				<div class="clearfix"></div>
			</div>
			<?php if($mobileView && !$detect->isTablet()) : ?>
				<script type="text/javascript">
							jQuery('body').scrollTop(0);
				</script>
			<?php endif; ?>			
		</div>
	</div>
</div>
	<script>

		//list_1-------------------------------------------------
			jQuery('.list_1 > li > a').hover(function(){
			jQuery(this).stop().animate({color:'#ff554e', marginLeft:5}, 300, "easeOutCubic")
			}, function(){;
			jQuery(this).stop().animate({color:'#fff', marginLeft:0}, 300, "easeOutCubic");
			})   
		//end list_1-------------------------------------------------

		jQuery(function(){
			var kontaktUrl = location.pathname;
			if(kontaktUrl != "/kontakt.html") { if(jQuery('.partRight').hasClass('kontakt')) { jQuery('.partRight').removeClass('kontakt'); } } //default
			if(kontaktUrl == "/kontakt.html")  { jQuery('.partRight').addClass('kontakt'); }
		});

	</script>
