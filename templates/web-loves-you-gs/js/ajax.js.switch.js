/*
*Version: 2.0 - last update: 14.05.2013;
*Jquery version: 1.8.3;
*Author: Behaart;
*/

//var siteUrl = document.location.href;

(function($){
	jQuery.fn.ajaxJSSwitch=function(o){
		var customObject={
				contentHolder:".dynamicContent",
				activeClass:"activeLink",
				classMenu:".sf-menu",
				classSubMenu:".sub-menu",
				classSelectMenu:".select-menu",
				mainPage: "index.php",
				hoverClass:"hoverItem",
				webSite:"#wrapper",
				pagesLoader:"#pageLoader",
				noLoadAjax:"noAjaxLoad",
				topMargin:0,
				bottomMargin:0,
				topMarginMobileDevices:0,
				bottomMarginMobileDevices:0,
				bodyMinHeight:0,
				bodyMinHeightMobileDevices:480,
				fullHeight:false,
				deviceSize:767,
				errorPage:"404.html",
				delaySubMenuHide:0,
				pageInit:function(pages){},
				backToSplash:function(){},
				currPageAnimate:function(page){},
				prevPageAnimate:function(page){},
				pageLoadComplete:function(){},
				menuInit:function(classMenu, classSubMenu){},
				buttonOver:function(item){},
				buttonOut:function(item){},
				subMenuButtonOver:function(item){},
				subMenuButtonOut:function(item){},
				subMenuShow:function(item){},
				subMenuHide:function(item){}
			},
			MSIE9 = (jQuery.browser.msie) && (jQuery.browser.version == 9),
			MSIE8 = (jQuery.browser.msie) && (jQuery.browser.version == 8),
			xhr,
			animationComplete = true,
			currentPage = 0,
			currButton ,
			prevButton,
			contentHeight = 0,
			initLoad = false,
			webSiteHref = window.location.protocol+"//"+window.location.hostname+window.location.pathname,
			minHeight,
			currHref = webSiteHref;
			
		jQuery.extend(customObject, o);
//ajax settings
		jQuery.ajaxSetup({
			type: "get",
			cache:true,
			async:true,
			accepts: "html",
			dataType: "html",
			isLocal:true,
			crossDomain:false
		});
//initialization function	
		init();
		function init(){
//parsing DOM
			if(MSIE9){
				jQuery("html").css({"overflow-y":"scroll", "position":"absolute", "width":"100%"});
			}else{
				jQuery("body").css({"overflow-y":"scroll", "position":"absolute", "width":"100%"});
			}
			jQuery("html, body").css({"height":"100%"});
			if(jQuery(customObject.contentHolder).contents().length==0){
				jQuery(customObject.contentHolder).html("<div class='content'></div>");
			}else{
				jQuery(customObject.contentHolder).contents().wrapAll("<div class='content'></div>");
			}
			jQuery(customObject.contentHolder).after(jQuery(customObject.contentHolder).clone().html("<div class='content'></div>"));
			jQuery(customObject.webSite).css({"position":"absolute", "width":"100%", "height":"100%", "overflow":"hidden"}).append("<div id='scriptHolder'></div>");
			jQuery(customObject.classSelectMenu).css({display:"none"});
			jQuery(customObject.contentHolder).eq(1).css({visibility:"hidden"});
			if(jQuery(customObject.contentHolder).eq(currentPage).find(".content").html().length==0){
				jQuery(customObject.contentHolder).eq(currentPage).css({visibility:"hidden"});
			}
			jQuery("head").append('<style>#wrapper section {overflow:visible!important}</style>')
			minHeight =  customObject.bodyMinHeight;
//call user function			
			customObject.menuInit(jQuery(customObject.classMenu), jQuery(customObject.classSubMenu));
			customObject.pageInit(jQuery(customObject.contentHolder));
//if request URL - reload page
			if(location.protocol!="file:"){
				if(location.search.replace("/?", "/")){
					var requestUrl = history.location || document.location;
					initLoad = true;
					loadContent(requestUrl.href.replace("/?", "/"), true);
				}else{
					currButton=jQuery("a[href*='"+jQuery("h1>a").attr("href")+"']").parent();
					changeActiveButton();
					customObject.backToSplash();
					setTimeout(wrapperResize, 500);
					customObject.pageLoadComplete();
				}
			}
//add events
			jQuery(window).bind('popstate', popStateHcange)//.bind('resize', wrapperResize)//.scroll(windowScroll);
			jQuery("a, area", customObject.webSite).live("click", followLink);
			jQuery(".select-menu").live("change", followLink);
			jQuery("#searchButton", ".search").live("click", searchButton);
			jQuery("input[name='s']", ".search").live("keypress", pressKey);
			jQuery("li", customObject.classMenu).hover(menuButtonOver, menuButtonOut).mousedown(menuButtonOver).trigger("mouseout");
			jQuery("*").not("a, li, ul").click('click', function(){})
			customObject.subMenuHide(jQuery("li", customObject.classMenu).find(">ul"));
		}
		function followLink(){
			if(location.protocol!="file:" && !jQuery(this).hasClass(customObject.noLoadAjax)){
				var linkHref,
					hrefDomenSlice,
					hrefDomen,
					followLinkTest,
					linkHrefLower,
					splash;
		
				if(jQuery(this).hasClass("select-menu")==false){
					linkHref = this.href;
				}else{
					linkHref = jQuery(this).val();
				}
				if(linkHref.indexOf("#")==-1){
					linkHrefLower = linkHref.toLowerCase();
					followLinkTest = linkHrefLower.indexOf(".html")!=-1 || linkHrefLower.indexOf(".htm")!=-1 || linkHrefLower.indexOf(".php")!=-1;
				 	splash = linkHref!=window.location.href+customObject.mainPage;
					if(jQuery(this).parents("form").length==0){
						hrefDomenSlice = linkHref.slice(linkHref.indexOf("://")+3, linkHref.length),
						hrefDomen = hrefDomenSlice.slice(0, hrefDomenSlice.indexOf("/"));
						if(webSiteHref.indexOf(hrefDomen)!=-1 && followLinkTest && linkHref.indexOf("assets")==-1){
							if(linkHref!=currHref && linkHref.indexOf(jQuery(">a", currButton).attr("href"))==-1 && animationComplete && splash){
								currHref = linkHref;
								loadContent(linkHref, true);
								if(jQuery(this).parents("li", customObject.classMenu).length!=0){
									jQuery(this).addClass(customObject.activeClass);
								}
							}
							return false;
						}
					}else{
						return false;	
					}
				}else{
					return false;
				}
			}	
		}
		function searchButton(){
			if(location.protocol!="file:"){
				if(animationComplete){
					var formAction = jQuery(this).parents("form").attr("action"),
						searchVal = jQuery(this).parents("form").find("input[name='s']").val();	
					jQuery(this).parent().find("input[type=text]").val("Search:");
					currHref = formAction+"?s="+searchVal;
					loadContent(formAction+"?s="+searchVal, true);
				}
				return false;
			}
		}
		function pressKey(e){
			if(location.protocol!="file:"){
				if(e.keyCode==13){
					if(animationComplete){
						var formAction = jQuery(this).parents("form").attr("action"),
							searchVal = jQuery(this).val();	
						jQuery(this).val("Search:").blur();
						loadContent(formAction+"?s="+searchVal, true);
					}
					return false;
				}
			}
		}
		function changeActiveButton(){
			if(jQuery("option[value*='"+jQuery(">a", currButton).attr("href")+"']").length==0){
				jQuery("option", ".select-menu").eq(0).attr({"selected":true});
			}else{
				jQuery("option[value*='"+jQuery(">a", currButton).attr("href")+"']").attr({"selected":true});
			}
			if(jQuery(">a", prevButton).parents("li", customObject.classMenu).length!=0){
				if(jQuery(">a", prevButton).length!=0){
					if(prevButton.parent(customObject.classMenu).length!=0){
						customObject.buttonOut(jQuery(">a",prevButton));
					}else{
						customObject.subMenuButtonOut(jQuery(">a",prevButton));
					}
					jQuery(prevButton, "ul").find(">a").removeClass(customObject.activeClass);
				}
			}
			currButton = currButton.not(currButton.find("."+customObject.noLoadAjax).parent());
			if(currButton && currButton!=""){
				currButton = jQuery("ul").find(currButton);
				if(jQuery(">a", currButton).parents("li", customObject.classMenu).length!=0){
					if(!jQuery(">a", currButton).hasClass(customObject.activeClass)){
						if(currButton.parent(customObject.classMenu).length!=0){
							customObject.buttonOver(jQuery(">a", currButton));
						}else{
							customObject.subMenuButtonOver(jQuery(">a", currButton));
						}
						jQuery(">a", currButton).addClass(customObject.activeClass);
					}
				}
			}
		}
		function menuButtonOver(){
			var item = jQuery(">a", this);
			if(!item.hasClass(customObject.activeClass)){
				if(item.parent().parent(customObject.classMenu).length!=0){
					customObject.buttonOver(item);
				}else{
					customObject.subMenuButtonOver(item);
				}
			}
			item.addClass(customObject.hoverClass)
			jQuery(".openSubMenu").removeClass("openSubMenu")
			customObject.subMenuShow(item.parent().find(">ul"));
			item.parent().find(">ul").addClass("openSubMenu");
		}
		function menuButtonOut(){
			var item = jQuery(">a", this);
			if(!item.hasClass(customObject.activeClass)){
				if(item.parent().parent(customObject.classMenu).length!=0){
					customObject.buttonOut(item);
				}else{
					customObject.subMenuButtonOut(item);
				}
			}
			setTimeout(function(){
				if(!item.hasClass(customObject.hoverClass)){
					customObject.subMenuHide(item.parent().find(">ul"));
					item.parent().find(">ul").removeClass("openSubMenu");
				}
			}, customObject.delaySubMenuHide)
			item.removeClass(customObject.hoverClass)
		}
		function popStateHcange(){
			if(location.protocol!="file:"){
				var requestUrl = history.location || document.location;
				if(!initLoad){
					currHref = window.location.protocol+"//"+window.location.hostname+window.location.pathname;
					loadContent(requestUrl.href.replace("/?", "/"));
				}				
			}
		}
		function loadContent(url, push){
				if(customObject.errorPage!=url){
					currentPage = currentPage==0 ? 1 : 0;
				}
				if (xhr)xhr.abort();
				xhr = jQuery.ajax({
					url: url,
					beforeSend:function(){
						var prevHolder = jQuery(customObject.contentHolder).not(jQuery(customObject.contentHolder).eq(currentPage))
						animationComplete = false;
						prevButton = jQuery("."+customObject.activeClass).parent();
						currButton = ""
						if(url.indexOf("search.php")==-1){
							currButton=jQuery("a[href='"+url.slice(url.lastIndexOf("/")+1, url.length)+"']").parent();
						}else{
							currButton = jQuery(".search").find("a");
						}
						jQuery("option", ".select-menu").eq(0).attr({"selected":true});
						changeActiveButton();
						jQuery(customObject.contentHolder).find("*").unbind();
						prevHolder.removeClass("currentHolder")
						customObject.prevPageAnimate(prevHolder);
						prevHolder.queue(prevPageAnonateComplete);
						jQuery(customObject.pagesLoader).stop(true).fadeIn(400);
					},
					success: function(data, textStatus, xhr) {
						var dataPage=jQuery(data),
							titlePage = dataPage.filter("title").text(),
							getContent = dataPage.find(customObject.contentHolder).contents(),
							currentHolder = jQuery(customObject.contentHolder).eq(currentPage);	
						if(url.indexOf("results.php")!=-1){
							getContent = dataPage.filter(customObject.contentHolder).contents();		
						}
						document.title = titlePage;
						if (push){history.pushState(null, null, url)};
						currentHolder.addClass("currentHolder").stop(true).css({visibility:"visible"}).find(".content").html(getContent);
						animationComplete = true;
						if(url.indexOf("search.php")==-1){
							parseHTML(data);
						}
						setTimeout(function(){
							initLoad = false;
						}, 500)
						setTimeout(function(){
							currentHolder.find(">.content").bind("resize.rainbows", wrapperResize).trigger("resize.rainbows");				
						}, 100)
						customObject.pageLoadComplete();
						customObject.currPageAnimate(currentHolder);
						currentHolder.queue(currPageAnonateComplete);					
						if(webSiteHref==url || url.lastIndexOf(customObject.mainPage)!=-1){
							if(currentHolder.find(".content").html().length==0){
								currentHolder.css({visibility:"hidden"});
							}
							customObject.backToSplash();
						}
						jQuery(customObject.pagesLoader).stop(true).fadeOut(400);
					},
			  		error:function() {
			  			loadContent(customObject.errorPage, true);
			  		}
				});
		}
		function prevPageAnonateComplete(){
			animationComplete = true;
			jQuery(customObject.contentHolder).not(jQuery(customObject.contentHolder).eq(currentPage)).css({visibility:"hidden", height:0}).find(".content").contents().remove();
			//customObject.pageLoadComplete();
		}
		function currPageAnonateComplete(){
			
		}
		function wrapperResize(){
			var margins = customObject.topMargin+customObject.bottomMargin,
				bodyChange = jQuery("body"),
				contentHeight = jQuery(customObject.contentHolder).eq(currentPage).outerHeight(),
				addContentHeight = jQuery(customObject.contentHolder).eq(currentPage).find(">.content").outerHeight()+parseInt(jQuery(customObject.contentHolder).eq(currentPage).css("padding-top"))+parseInt(jQuery(customObject.contentHolder).eq(currentPage).css("padding-bottom")),
				noFullContent=addContentHeight+margins<jQuery(window).height();
				
			if(MSIE9){
				bodyChange = jQuery("html");
			}
			if(jQuery(document).outerWidth()<customObject.deviceSize){
				margins = customObject.topMarginMobileDevices+customObject.bottomMarginMobileDevices;
				minHeight = customObject.bodyMinHeightMobileDevices;
			}else{
				minHeight = customObject.bodyMinHeight
			}
			if(noFullContent && customObject.fullHeight){
				jQuery(customObject.contentHolder).eq(currentPage).height(jQuery(window).height()-margins-parseInt(jQuery(customObject.contentHolder).eq(currentPage).css("padding-top"))-parseInt(jQuery(customObject.contentHolder).eq(currentPage).css("padding-bottom"))-2)
			}else{
				jQuery(customObject.contentHolder).eq(currentPage).height("auto")
				jQuery(customObject.contentHolder).eq(currentPage).parents("section").stop(true).animate({"height":jQuery(customObject.contentHolder).eq(currentPage).find(">.content").outerHeight()}, 700, "easeInOutCubic");
			}
			contentHeight = jQuery(customObject.contentHolder).eq(currentPage).outerHeight()+margins;
			if(contentHeight>minHeight){
				if(jQuery(document).outerWidth(true)<customObject.deviceSize){
					bodyChange.stop(true).css({"min-height":contentHeight});
				}else{
					bodyChange.stop(true).animate({"min-height":contentHeight}, 700, "easeInOutCubic");
				}
			}else{
				bodyChange.stop(true).animate({"min-height":minHeight}, 700, "easeInOutCubic");
			}
		}
		function windowScroll(){
			var bodyChange = jQuery("body");
			if(MSIE9){
				bodyChange  = jQuery("html");
			}
			jQuery("body, html").stop(true, true);
		}
		function parseHTML(data){
			var sliceBody = data.slice(data.indexOf("<body>"), data.lastIndexOf("</body>")),
				res = "";

			jQuery(sliceBody).filter('script').each(function(){
				var scriptHTML =jQuery(this)[0].outerHTML;
					if(scriptHTML.indexOf("bootstrap")==-1){
				        res+=scriptHTML;
			        }
			    })
			jQuery("#scriptHolder").css({"display":"none"}).html(res);
		}
	}
})(jQuery)