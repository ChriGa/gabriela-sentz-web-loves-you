var splash = true;
var first_time = 0;
var time_out;
var time_out2;

jQuery(document).ready(function(){	


var MSIE8 = (jQuery.browser.msie) && (jQuery.browser.version == 8);

	jQuery.fn.ajaxJSSwitch({

		topMargin:100,//mandatory property for decktop

		bottomMargin:200,//mandatory property for decktop

		bodyMinHeight: 980,

		topMarginMobileDevices:0,//mandatory property for mobile devices

		bottomMarginMobileDevices:0,//mandatory property for mobile devices

		menuInit:function (classMenu, classSubMenu){
			//cg: nur ab 768 wdith function ausführen!:
			var windowsize = jQuery(window).width();
			if (windowsize > 767) {
				classMenu.find(">li").each(function(){

					var conText = jQuery(this).find('.mText').text();

					jQuery(">a", this).append("<div class='_area'></div><div class='_overPl'></div><div class='_overLine'></div><div class='mTextOver'>"+conText+"</div>"); 

				})
			}

		},

		buttonOver:function (item){

			/*$(".mText",item).stop(true).animate({top:"160px"}, 600, 'easeOutCubic');*/

			jQuery(".mText",item).stop(true).animate({opacity:0}, 600, 'easeOutCubic');

            jQuery(".mTextOver", item).stop(true).delay(150).animate({opacity:1}, 500, 'easeOutCubic');

			jQuery("._overPl", item).stop(true).animate({opacity:0}, 500, 'easeOutCubic');

			jQuery("._overLine", item).stop(true).animate({opacity:1}, 500, 'easeOutCubic');

		},

		buttonOut:function (item){

			/*$(".mText", item).stop(true).animate({top:"0px"}, 600, 'easeOutCubic');*/

			jQuery(".mText",item).stop(true).animate({opacity:1}, 600, 'easeOutCubic');

			jQuery(".mTextOver", item).stop(true).delay(20).animate({opacity:0}, 400, 'easeOutCubic');

			jQuery("._overPl", item).stop(true).animate({opacity:1}, 400, 'easeOutCubic');

			jQuery("._overLine", item).stop(true).animate({opacity:0}, 500, 'easeOutCubic');

		},

		subMenuButtonOver:function (item){

			

		},

		subMenuButtonOut:function (item){

		

		},

		subMenuShow:function(subMenu){

			if(MSIE8){

				//subMenu.css({"display":"block", "margin-top":-(subMenu.outerHeight()+0)});

				subMenu.css({"display":"block", "margin-top":0});

			}else{

				//subMenu.stop(true).css({"display":"block", "margin-top":-(subMenu.outerHeight()+0)}).animate({"opacity":"1"}, 600, "easeInOutCubic");

				subMenu.stop(true).css({"display":"block", "margin-top":0}).animate({"opacity":"1"}, 600, "easeInOutCubic");

			}

		},

		subMenuHide:function(subMenu){

			if(MSIE8){

				subMenu.css({"display":"none"});

			}else{

				subMenu.stop(true).delay(300).animate({"opacity":"0"}, 600, "easeInOutCubic", function(){

					jQuery(this).css({"display":"none"})

				});

			}

		},

		pageInit:function (pages){

		},

		currPageAnimate:function (page){



			jQuery('header').css({'z-index':500});

            jQuery('#wrapper section').css({'z-index':600});



            jQuery('.content').stop(true).delay(100).css({'minHeight':'628px'});

              
            //CG overide für smartphone - ul li's sollen auf Smartphone NICHT -700 top animiert werden weil im canvas sichtbar mit resp-Menu bleiben muss
        	var windowsize = jQuery(window).width();
        	if(windowsize > 767) {
              jQuery('.sf-menu > li').each(

                    function(){
                        jQuery(this).stop().delay(jQuery(this).index()*100).animate({top:-700}, 600, "easeInOutCubic");

                    }

              )
            }



             /*moveSplash();*/

             if(first_time == 0){
             	time_out = 2200;
             	time_out2 = 2800;
             }else{
             	time_out = 700;
             	time_out2 = 1500;
             }

			setTimeout(function() {

				if(!splash){

			             jQuery('.mainBg').find(".partRight").stop(true).delay(100).animate({"left":"260px"}, 600, "easeOutBack");

			             jQuery('.mainBg').find(".partLeft").stop(true).delay(200).animate({"left":"-89px"}, 600, "easeOutBack");

			             jQuery('.mainBg').find(".partMenu").stop(true).delay(300).animate({"left":"-39px"}, 600, "easeOutBack");

			         }



			         setTimeout(function() {

			         	if(MSIE8){

			         		page.css({"visibility": "visible"});

			         	}

			         },800);

			         first_time = 1;	

			},time_out);



             page.css({"opacity": 0}).stop(true, true).delay(time_out2).animate({"opacity":1}, 1500, "easeOutCubic");



             if(MSIE8){

             	page.css({"visibility": "hidden"});

             }


//CG removeClass muss innerhalb des ajax contents handler bzw einer der Funktionen geladen werden! 
	jQuery('.bckBut, #logo').click(function() {
		jQuery('.bckgr-image').removeClass('blur');
	});	

		},

		prevPageAnimate:function (page){



			splash = false;



			page.stop(true, true).animate({"display":"block", "opacity": 0}, 400, "easeInCubic",function(){

				jQuery('.mainBg').find(".partRight").stop(true).delay(0).animate({"left":windowW()}, 600, "easeOutBack");

				jQuery('.mainBg').find(".partLeft").stop(true).delay(0).animate({"left":-windowW()}, 600, "easeOutBack");

				jQuery('.mainBg').find(".partMenu").stop(true).delay(0).animate({"left":-windowW()}, 600, "easeOutBack");

			})



		},

		backToSplash:function (){



			splash = true;



			 jQuery('header').css({'z-index':600});

            jQuery('#wrapper section').css({'z-index':500});



            jQuery('.content').stop(true).delay(100).css({'minHeight':'0px'});




            var windowsize = jQuery(window).width();
            	if(windowsize > 599) {
		             jQuery(".sf-menu").find(".item01").delay(500).stop().animate({"top":"-30px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item02").delay(400).stop().animate({"top":"190px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item03").delay(300).stop().animate({"top":"207px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item04").delay(200).stop().animate({"top":"359px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item05").delay(100).stop().animate({"top":"494px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item06").delay(150).stop().animate({"top":"320px", "opacity": "1"}, 700, "easeOutBack");

		             jQuery(".sf-menu").find(".item07").delay(50).stop().animate({"top":"150px", "opacity": "1"}, 700, "easeOutBack");                    
            }
		},

		pageLoadComplete:function (){

		// CG respsonsive Menue nach laden des Ajax Container schliessen:
			jQuery('#toggle').removeClass('active');
			jQuery('#overlay, .navbarTitle, #wrapper').removeClass('open');
		// END 
	
			jQuery('.but_back > a').find("span").stop().css({opacity:0});



			jQuery('.but_back > a').hover(function(){

				jQuery(this).find("span").stop().animate({opacity:1}, 500, "easeInOutCubic");

				jQuery(this).stop().animate({color:"#db2533"}, 500, "easeInOutCubic");

				}, function(){;

				jQuery(this).find("span").stop().animate({opacity:0}, 500, "easeInOutCubic");

				jQuery(this).stop().animate({color:"#fff"}, 500, "easeInOutCubic");

			});



			setTimeout(function() {







				 if (jQuery('.scroll>div').height()>jQuery('.scroll').height()) {

                    jQuery('.scroll')

                	.uScroll({			

                		mousewheel:true,

                        step: 148,

                        lay:'outside'

                	});

                } else {

                    jQuery('.viz').css('display','none')

                }









				if (jQuery('.scroll_1>div').height()>jQuery('.scroll_1').height()) {

                    jQuery('.scroll_1')

                	.uScroll({			

                		mousewheel:true,

                        step: 148,

                        lay:'outside'

                	});

                } else {

                    jQuery('.viz_1').css('display','none')

                }



			},300);



		},

	});

	

})



function moveSplash() {

    jQuery('.mainBg').find('.content_bg, >div>div').each(function (ind){

        jQuery(this)

        .stop().delay(getRandomFromRange(0,400))

        .animate({'left': getRandomFromRange(-windowW(),windowW()),

              'top': getRandomFromRange(-windowH(),windowH())

        },

            getRandomFromRange(400,400), 'easeInCubic')

        .animate({'left': coords[ind].x, 'top': coords[ind].y}, 

            getRandomFromRange(0,800), 'easeOutElastic')

    });

}





function initSplash(){



    jQuery('.mainBg').find(".partRight").css({"left":windowW()});

    jQuery('.mainBg').find(".partLeft").css({"left":-windowW()});

    jQuery('.mainBg').find(".partMenu").css({"left":-windowW()});



}



jQuery(window).load(function(){	

	jQuery("#webSiteLoader").delay(500).animate({opacity:0}, 600, "easeInCubic", function(){jQuery("#webSiteLoader").remove()});



	jQuery(".image_resize").image_resize({align_img:"center", mobile_align_img:"center"});


	initSplash();


	});