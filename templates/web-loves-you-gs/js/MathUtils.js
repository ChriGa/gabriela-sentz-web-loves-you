function getRandomFromRange(min, max){
  return Math.random() * (max - min) + min;
}

function getRandomFromRangeInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function DegToRad(deg) {
    return (Math.PI/180)*deg;
} 

function RadToDeg(rad) {
    return (180/Math.PI)*rad;
}

function getXOnCircle(radius_,angle_){
    return (radius_* Math.cos(DegToRad(angle_)));
}

function getYOnCircle(radius_,angle_){
    return (radius_* Math.sin(DegToRad(angle_)));
}
function windowH() {
	return ((jQuery(window).height()>=parseInt(jQuery('body').css('minHeight')))?jQuery(window).height():parseInt(jQuery('body').css('minHeight')));
}
function windowW() {
	return ((jQuery(window).width()>=parseInt(jQuery('body').css('minWidth')))?jQuery(window).width():parseInt(jQuery('body').css('minWidth')));
}
function windowHalfH() {
	return (windowH()*.5);
}
function windowHalfW() {
	return (windowW()*.5);
}