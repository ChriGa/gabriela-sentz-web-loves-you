<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header>
	<div class="container">
      <div class="row ">
		<div class="span12">
		<?php if(!$detect->isMobile()) : ?>          		
			<h1 class="logoH1"><a id="logo" class="brand" href="<?php echo $this->baseurl ."index.php"; ?>">
				<span>Gabriela <span>Sentz</span></span> <br />
				Brautstyling &amp; Kosmetik
			</a></h1>
				<img class="lotusLogo" src="templates/web-loves-you-gs/img/lotus_logo.png" alt="Gabriela Sentz Logo" />		               
		<?php else : ?>
			<div class="mobile-logo">
				<h1 class="logoH1 mobile"><a id="logo" class="brand" href="<?php echo $this->baseurl ."index.php"; ?>">
					<span>Gabriela <span>Sentz</span></span> <br />
					<span class="logoBuiz">Brautstyling &amp; Kosmetik</span>
				</a></h1>
					<img class="lotusLogo-mobile" src="templates/web-loves-you-gs/img/lotus_logo.png" alt="Gabriela Sentz Logo" />
			</div>
		<?php endif; ?>
			<div class="menuMov <?php print ($detect->isMobile() ? "mobile mobMenu" : "desktop") ?>">
				<div class="menu">
					<?php /*
		            <button type="button" class="btn btn-navbar collapsed hidden-desktop" data-toggle="collapse" data-target=".nav-collapse">
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		            </button>
		            */ ?>           	                         
				  	<?php if ($this->countModules('menu') && !$detect->isMobile() || $detect->isTablet() ) : ?>
						<jdoc:include type="modules" name="menu" style="none" />
					<?php else: ?>
					        <div class="navbar">
					        	<h2 class="navbarTitle">men&uuml;</h2>						     
								<div class="button_container" id="toggle">
									<span class="top"></span>
									<span class="middle"></span>
									<span class="bottom"></span>
								</div>         		                         						 					      
						    </div>
							<jdoc:include type="modules" name="menu-mobile" style="none" />
					<?php endif; ?>	
				</div>
			</div>
		</div>
      </div>
    </div>
</header>