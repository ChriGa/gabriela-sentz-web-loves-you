<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<!--content-->	
		<div class="containerHolder">
    		<section>
                <div class="container">
                    <div class="dynamicContent">
<?php	
$app = JFactory::getApplication();
$menu = $app->getMenu();
$is_frontpage = false;
		if ($menu->getActive() == $menu->getDefault()) { $is_frontpage = true; }
		if(!$is_frontpage) { ?>
						<!-- Begin Content -->
						<jdoc:include type="modules" name="position-3" style="xhtml" />
						<jdoc:include type="message" />
						<jdoc:include type="component" />
					<?php if(!$detect->isMobile()) : ?>
             			<jdoc:include type="modules" name="position-2" style="none" />
             		<?php endif; ?>
           <?php } ?>
                    </div>
                </div>
    		</section>
		</div>	
<!-- End Content -->    