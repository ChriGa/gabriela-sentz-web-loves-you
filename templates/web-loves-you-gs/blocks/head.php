<?php
/**
 * @author   	0911webdesgin.de
 * @copyright   Copyright (C) 2016 0911webdesgin.de. All rights reserved.
 * @URL 		https://0911webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;

// Add JavaScript Frameworks
//JHtml::_('bootstrap.framework');
//$doc->addScript('templates/' . $this->template . '/js/template.js');
/*$doc->addScript('templates/' . $this->template . '/js/include_script.js');

if ($detect->isMobile()) {
	$doc->addScript('templates/' . $this->template . '/js/script-mobile.js');
	$doc->addScript('templates/' . $this->template . '/js/jquery.backstretch.min.js');
} else {
	$doc->addScript('templates/' . $this->template . '/js/script.js');
}*/

// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/normalize.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/bootstrap.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/style.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');


// Load optional RTL Bootstrap CSS
//JHtml::_('bootstrap.loadCss', true, $this->direction);

// Add Stylesheets - CG overrides als letztes!!
$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');

?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
		<style type="text/css">
			h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
			}
		</style>
	<?php endif; ?>

	<link rel="canonical" href="<?php print JUri::base(); ?>" />
	
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->

<?php /* google analytics ausblenden vorerst seit dsgvo 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90545775-1', 'auto');
  ga('send', 'pageview');

</script>
*/ ?>