<?php
/**
 * @author   	0911webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>	
<?php //CG:  folgende Elemnte müssen evtl abhängig Mobil vs Desktop alternativ ausgegeben werden ?>
<?php
/*<div class="mainPicHolder">
	<div class="container">
		<div class="row">
			<div class="span6 offset3">
				<div class="mainPic"></div>
			</div>
		</div>
	</div>
*/	?>
<?php if(!$detect->isMobile()) : ?>
	<div class="mainPicHolder">
		<? //xml version="1.0" encoding="utf-8"?>
		<div class="bloc bloc-head">
			<?php print file_get_contents(JPATH_ROOT . "/templates/" . $this->template . "/assets/abstractBckgr.svg"); ?>
		</div>
	</div>
<div class="contHolder">
	<div class="container">
		<div class="mainBg">
                <div class="content_bg"></div>
                <div>
                    <div class="partLeft"></div>
                    <div class="partRight"></div>
                    <div class="partMenu"></div>
                </div>
            </div>
    	</div>
    </div> 
<?php endif; ?>
