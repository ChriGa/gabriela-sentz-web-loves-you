<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer>
	<div class="container">				
		<div class="row">							
			<?php if ($this->countModules('footnav')) : ?>
				<div class="span8 footnav">
					<div class="module_footer position_footnav">
						<jdoc:include type="modules" name="footnav" style="none" />
					</div>			
				</div>
			<?php endif ?>						
			<div class="span12">
				<?php /*
				<ul class="social-icon">
					<li><a href="https://www.facebook.com/WellnessKosmetikGabriela/" target="_blank" title="Gabriela Sentz Kosmetik auf Facebook">
						<img class="animate" src="<?php echo JURI::base().'images/' ?>fb_logo.png" alt="Facebook Logo Gabriela Sentz Brautstyling">
					</a></li>
				</ul> */ ?>
				<a class="impLink impLink--impress" href="/impressum.html">Impressum</a>
				<a class="impLink impLink--daten" href="/datenschutz.html">Datenschutz</a>				
			</div>	
			<div class="clearfix"></div>	
		</div>		
	</div>
</footer>